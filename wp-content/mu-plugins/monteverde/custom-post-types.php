<?php defined('ABSPATH') or die;

require_once __DIR__ . '/helpers/class-vm-meta-box.php';

add_action('init', function() {

	/* Places */

	register_post_type('place', [
		'labels' => [
			'name' => _x('Places', 'post type general name', 'monteverde'),
			'singular_name' => _x('Place', 'post type singular name', 'monteverde'),
			'menu_name' => _x('Places', 'admin menu', 'monteverde'),
			'name_admin_bar' => _x('Place', 'add new on admin bar', 'monteverde'),
			'add_new' => _x('Add new', 'place', 'monteverde'),
			'add_new_item' => __('Add New Place', 'monteverde'),
			'new_item' => __('New Place', 'monteverde'),
			'edit_item' => __('Edit Place', 'monteverde'),
			'view_item' => __('View Place', 'monteverde'),
			'all_items' => __('All Places', 'monteverde'),
			'search_items' => __('Search Places', 'monteverde'),
			'parent_item_color' => __('Parent Places:', 'monteverde'),
			'not_found' => __('No places found.', 'monteverde'),
			'not_found_in_trash' => __('No places found in Trash', 'monteverde'),
		],
		'description' => __('Places custom post type for Monteverde Engine.'),
		'public' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-location',
		'capability_type' => 'page',
		'supports' => [
			'title',
			'editor',
			'thumbnail'
		],
		'rewrite' => [
			'slug' => 'place-lugar'
		]
	]);

	VM_MetaBox::registerForCustomPostType('place', [
		'id' => 'place-information',
		'title' => __('Place Information', 'monteverde'),
		'context' => 'normal',
		'priority' => 'default',

		'group-options' => [
			'sustainable-tourism' => [
				'label' => __('Sustainable Tourism', 'monteverde')
			],
			'social-networks' => [
				'label' => __('Social Networks', 'monteverde')
			],
			'lodging' => [
				'label' => __('Lodging only', 'monteverde'),
				'collapsible' => true
			],
			'book-now' => [
				'label' => __('"Book Now" capabilities', 'monteverde'),
				'collapsible' => true
			],
			'extra' => [
				'label' => __('Extras', 'monteverde'),
				'collapsible' => true
			],
		],

		'fields' => [

			'short-description' => [
				'label' => __('Short Description', 'monteverde'),
				'note' => sprintf(__('%s char.'), 2000),
				'type' => 'longtext',
				'maxlength' => 2000
			],

			'address' => [
				'label' => __('Physical Address', 'monteverde'),
				'type' => 'text'
			],

			'website' => [
				'label' => __('Website URL', 'monteverde'),
				'type' => 'text'
			],

			'email' => [
				'label' => __('Email Address', 'monteverde'),
				'type' => 'text'
			],

			'phone' => [
				'label' => __('Phone', 'monteverde'),
				'type' => 'text'
			],

			'fax' => [
				'label' => __('Fax', 'monteverde'),
				'type' => 'text'
			],

			'price-range' => [
				'label' => __('Price Range', 'monteverde'),
				'type' => 'enum',
				'values' => [
					1 => '$',
					2 => '$$',
					3 => '$$$',
					4 => '$$$$',
				]
			],

			'features' => [
				'label' => __('Features', 'monteverde'),
				'type' => 'longtext'
			],

			'facebook' => [
				'label' => __('Facebook Profile', 'monteverde'),
				'note' => __('full URL', 'monteverde'),
				'type' => 'text',
				'group' => 'social-networks'
			],

			'twitter' => [
				'label' => __('Twitter Profile', 'monteverde'),
				'note' => __('full URL', 'monteverde'),
				'type' => 'text',
				'group' => 'social-networks'
			],

			'google-plus' => [
				'label' => __('Google+ Profile', 'monteverde'),
				'note' => __('full URL', 'monteverde'),
				'type' => 'text',
				'group' => 'social-networks'
			],

			'sustainable-tourism' => [
				'label' => _x('Level', 'sustainable tourism rating', 'monteverde'),
				'type' => 'enum',
				'values' => [
					1 => sprintf(_n('%s leaf', '%s leaves', 1, 'monteverde'), 1),
					2 => sprintf(_n('%s leaf', '%s leaves', 2, 'monteverde'), 2),
					3 => sprintf(_n('%s leaf', '%s leaves', 3, 'monteverde'), 3),
					4 => sprintf(_n('%s leaf', '%s leaves', 4, 'monteverde'), 4),
					5 => sprintf(_n('%s leaf', '%s leaves', 5, 'monteverde'), 5),
				],
				'group' => 'sustainable-tourism'
			],

			'sustainable-tourism-link' => [
				'label' => __('URL', 'monteverde'),
				'type' => 'text',
				'group' => 'sustainable-tourism'
			],

			'stars' => [
				'label' => _x('Stars', 'hotel rating system', 'monteverde'),
				'type' => 'enum',
				'note' => __('if applicable', 'monteverde'),
				'values' => [
					1 => sprintf(_n('%s star', '%s stars', 1, 'monteverde'), 1),
					2 => sprintf(_n('%s star', '%s stars', 2, 'monteverde'), 2),
					3 => sprintf(_n('%s star', '%s stars', 3, 'monteverde'), 3),
					4 => sprintf(_n('%s star', '%s stars', 4, 'monteverde'), 4),
					5 => sprintf(_n('%s star', '%s stars', 5, 'monteverde'), 5),
				],
				'group' => 'lodging'
			],

			'book-now' => [
				'label' => __('Enabled', 'monteverde'),
				'type' => 'boolean',
				'group' => 'book-now'
			],

			'book-now-address' => [
				'label' => __('Forwarding Email Address', 'monteverde'),
				'note' => __('optional', 'monteverde'),
				'type' => 'text',
				'group' => 'book-now'
			],

			'act-7600' => [
				'label' => __('Act 7600', 'monteverde'),
				'tooltip' => __('Equal opportunities for persons with disabilities'),
				'type' => 'boolean',
				'group' => 'extra'
			],

		]
	]);

	/* Photos */

	register_post_type('photo', [
		'labels' => [
			'name' => _x('Photos', 'post type general name', 'monteverde'),
			'singular_name' => _x('Photo', 'post type singular name', 'monteverde'),
			'menu_name' => _x('Photos', 'admin menu', 'monteverde'),
			'name_admin_bar' => _x('Photo', 'add new on admin bar', 'monteverde'),
			'add_new' => _x('Add new', 'photo', 'monteverde'),
			'add_new_item' => __('Add New Photo', 'monteverde'),
			'new_item' => __('New Photo', 'monteverde'),
			'edit_item' => __('Edit Photo', 'monteverde'),
			'view_item' => __('View Photo', 'monteverde'),
			'all_items' => __('All Photos', 'monteverde'),
			'search_items' => __('Search Photos', 'monteverde'),
			'parent_item_color' => __('Parent Photos:', 'monteverde'),
			'not_found' => __('No photos found.', 'monteverde'),
			'not_found_in_trash' => __('No photos found in Trash', 'monteverde'),
		],
		'description' => __('Photos custom post type for Monteverde Engine.'),
		'public' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-camera',
		'capability_type' => 'page',
		'supports' => [
			'title',
			'thumbnail'
		],
		'rewrite' => [
			'slug' => 'photo-foto'
		]
	]);

	add_action('add_meta_boxes_photo', function() {

		remove_meta_box('postimagediv', 'photo', 'side');
	    add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'photo', 'normal', 'high');

	});

	/* Maps */

	register_post_type('map', [
		'labels' => [
			'name' => _x('Maps', 'post type general name', 'monteverde'),
			'singular_name' => _x('Map', 'post type singular name', 'monteverde'),
			'menu_name' => _x('Maps', 'admin menu', 'monteverde'),
			'name_admin_bar' => _x('Map', 'add new on admin bar', 'monteverde'),
			'add_new' => _x('Add new', 'map', 'monteverde'),
			'add_new_item' => __('Add New Map', 'monteverde'),
			'new_item' => __('New Map', 'monteverde'),
			'edit_item' => __('Edit Map', 'monteverde'),
			'view_item' => __('View Map', 'monteverde'),
			'all_items' => __('All Maps', 'monteverde'),
			'search_items' => __('Search Maps', 'monteverde'),
			'parent_item_color' => __('Parent Maps:', 'monteverde'),
			'not_found' => __('No maps found.', 'monteverde'),
			'not_found_in_trash' => __('No maps found in Trash', 'monteverde'),
		],
		'description' => __('Maps custom post type for Monteverde Engine.'),
		'public' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-location-alt',
		'capability_type' => 'page',
		'supports' => [
			'title',
			'thumbnail'
		],
		'rewrite' => [
			'slug' => 'map-mapa'
		]
	]);

	add_action('add_meta_boxes_map', function() {

	    add_meta_box('map_attach_pdf', __('PDF File', 'monteverde'), function($post, $wp_metabox) {

	    	$pdf_url = null;

	    	if (!empty($post->ID)) {
	    		$pdf_url = get_post_meta($post->ID, 'map-attachment', true);
	    	}

	    	include __DIR__ . '/helpers/map-attach-pdf.php';

	    }, 'map', 'normal', 'high');

	});

	add_action('add_meta_boxes_map', function() {

		remove_meta_box('postimagediv', 'map', 'side');
	    add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'map', 'normal', 'high');

	});


	add_action('save_post', function($post_id) {

		global $post;

		if (
			!isset($_POST['meta_box_nonce-map-attach-pdf']) ||
			!wp_verify_nonce($_POST['meta_box_nonce-map-attach-pdf'], 'map-attach-pdf')
		) {
			return $post_id;
		}

		if (
			(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) ||
			isset($_REQUEST['bulk_edit'])
		) {
			return $post_id;
		}

		if (isset($post->post_type) && $post->post_type == 'revision') {
			return $post_id;
		}

		if (!current_user_can('edit_post', $post->ID)) {
			return $post_id;
		}

		if (empty($_POST['map-attach-pdf-file'])) {
			return $post_id;
		}

		update_post_meta($post->ID, 'map-attachment', $_POST['map-attach-pdf-file']);

		return $post_id;

	}, 10, 2);

});
