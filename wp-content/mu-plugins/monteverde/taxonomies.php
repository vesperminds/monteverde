<?php defined('ABSPATH') or die;

add_action('init', function() {

	/* Places categories */

	register_taxonomy('place_type', 'place', [
		'labels' => [
			'name' => _x('Place Types', 'taxonomy general name', 'monteverde'),
			'singular_name' => _x('Place Type', 'taxonomy singular name', 'monteverde'),
			'all_items' => __('All Place Types', 'monteverde'),
			'edit_item' => __('Edit Place Type', 'monteverde'),
			'view_item' => __('View Place Type', 'monteverde'),
			'update_item' => __('Update Place Type', 'monteverde'),
			'add_new_item' => __('Add New Place Type', 'monteverde'),
			'new_item_name' => __('New Place Type Name', 'monteverde'),
			'parent_item' => __('Parent Place Type', 'monteverde'),
			'parent_item_colon' => __('Parent Place Type:', 'monteverde'),
			'search_items' => __('Search Place Types', 'monteverde'),
			'popular_items' => __('Popular Place Types', 'monteverde'),
			'separate_items_with_commas' => __('Separate place types with commas', 'monteverde'),
			'add_or_remove_items' => __('Add or remove place types', 'monteverde'),
			'choose_from_most_used' => __('Choose from the most used place types', 'monteverde'),
			'not_found' => __('No place types found.', 'monteverde'),
			'menu_name' => _x('Place Types', 'taxonomy menu name', 'monteverde')
		],
		'public' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'hierarchical' => true,
		'rewrite' => [
			'slug' => 'places-lugares',
			'hierarchical' => true
		]
	]);
	register_taxonomy_for_object_type('place_type', 'place');

	/* Gallery */

	register_taxonomy('gallery', 'photo', [
		'labels' => [
			'name' => _x('Galleries', 'taxonomy general name', 'monteverde'),
			'singular_name' => _x('Gallery', 'taxonomy singular name', 'monteverde'),
			'all_items' => __('All Galleries', 'monteverde'),
			'edit_item' => __('Edit Gallery', 'monteverde'),
			'view_item' => __('View Gallery', 'monteverde'),
			'update_item' => __('Update Gallery', 'monteverde'),
			'add_new_item' => __('Add New Gallery', 'monteverde'),
			'new_item_name' => __('New Gallery Name', 'monteverde'),
			'parent_item' => __('Parent Gallery', 'monteverde'),
			'parent_item_colon' => __('Parent Gallery:', 'monteverde'),
			'search_items' => __('Search Galleries', 'monteverde'),
			'popular_items' => __('Popular Galleries', 'monteverde'),
			'separate_items_with_commas' => __('Separate galleries with commas', 'monteverde'),
			'add_or_remove_items' => __('Add or remove galleries', 'monteverde'),
			'choose_from_most_used' => __('Choose from the most used galleries', 'monteverde'),
			'not_found' => __('No galleries found.', 'monteverde'),
			'menu_name' => _x('Gallery', 'taxonomy menu name', 'monteverde')
		],
		'public' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'hierarchical' => false,
		'rewrite' => [
			'slug' => 'gallery-galeria',
			'hierarchical' => true
		]
	]);
	register_taxonomy_for_object_type('gallery', 'photo');

});

/* Helper functions */

function mv_tax_list($descriptors = null) {

	if (empty($descriptors)) {
		return [];
	}

	$output = [];

	foreach ($descriptors as $descriptor) {

		if (count($descriptor) !== 2) {
			continue;
		}

		$tax = $descriptor[0];
		$slug = $descriptor[1];

		$term = get_term_by('slug', $slug, $tax);

		if (empty($term)) {
			continue;
		}

		$data = [
			'name' => $term->name,
			'description' => $term->description,
			'image' => z_taxonomy_image_url($term->term_id, 'thumbnail')
		];

		$data['link'] = get_term_link($term->term_id, $tax);

		$output[] = $data;

	}

	return $output;

}

function mv_tax_link($tax, $slug) {

	static $tax_link_memoize;

	if (empty($tax)) {
		return '';
	}

	if (empty($slug)) {
		return '';
	}

	if (!is_array($tax_link_memoize)) {
		$tax_link_memoize = [];
	}
	elseif (isset($tax_link_memoize[$tax . '/' . $slug])) {
		return $tax_link_memoize[$tax . '/' . $slug];
	}

	$link = '';

	$term = get_term_by('slug', $slug, $tax);

	if (!empty($term)) {

		$link = get_term_link($term->term_id, $tax);

	}

	$tax_link_memoize[$tax . '/' . $slug] = $link;

	return $link;

}

function mv_tax_attachment($tax, $slug_or_id, $size = 'thumbnail') {

	static $tax_image_memoize;

	if (empty($slug_or_id)) {
		return '';
	}

	if (!is_array($tax_image_memoize)) {
		$tax_image_memoize = [];
	}
	elseif (isset($tax_image_memoize[$size . ':' . $tax . '/' . $slug_or_id])) {
		return $tax_image_memoize[$size . ':' . $tax . '/' . $slug_or_id];
	}

	$data = [
		'image' => null
	];

	if (is_numeric($slug_or_id)) {

		$id = $slug_or_id;
		$term = get_term_by('id', $id, $tax);

		if (!empty($term)) {
			$slug = $term->slug;
		}

	}
	else {
		$slug = $slug_or_id;
	}

	if (empty($slug)) {

		if (!empty($id)) {
			$tax_image_memoize[$size . ':' . $tax . '/' . $id] = $data;
		}

		return $data;

	}

	$title = '$taxonomy:' . $tax . '-' . $slug;

	$attachment = get_page_by_title($title, 'OBJECT', 'attachment');

	if (!empty($attachment)) {

		$data['image'] = wp_get_attachment_image_src($attachment->ID, $size);

	}

	$tax_image_memoize[$size . ':' . $tax . '/' . $slug] = $data;

	if (!empty($id)) {
		$tax_image_memoize[$size . ':' . $tax . '/' . $id] = $data;
	}

	return $data;

}
