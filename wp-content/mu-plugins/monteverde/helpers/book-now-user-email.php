Hey <?= $data['first_name'] ?>,

Thanks for your reservation to `<?= $data['place_name'] ?>`. We will get back to you as soon as possible.

- Monteverde staff.