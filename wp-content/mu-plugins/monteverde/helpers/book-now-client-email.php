MONTEVERDE
==========

New Booking
-----------

Place: <?= $data['place_name'] ?>

Place Email: <?= $place_email ?>


User Email: <?= $data['email'] ?>


First Name: <?= $data['first_name'] ?>

Last Name: <?= $data['last_name'] ?>

<?php if (!empty($data['last_name'])): ?>
Phone: <?= $data['last_name'] ?>

<?php endif; ?>

PAX:
- Adults: <?= $data['adults'] ?>

- Children: <?= $data['children'] ?>

- Children Ages: <?php if (!empty($data['children_age'])) {
    $ages = is_array($data['children_age']) ? $data['children_age'] : [ $data['children_age'] ];
    $ages_sanitized = [];

    foreach ($ages as $age) {
        $ages_sanitized[] = strip_tags($age);
    }

    echo implode(', ', $ages_sanitized);
} else { echo 'N/A'; } ?>


Arrival date: <?= $data['arrival_date'] ?>


<?php if (!empty($data['details'])): ?>
Details:
<?= $data['details'] ?>

<?php endif; ?>
