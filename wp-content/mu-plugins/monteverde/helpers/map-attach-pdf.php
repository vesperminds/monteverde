<?php defined('ABSPATH') or die; ?>

<?php wp_nonce_field('map-attach-pdf', 'meta_box_nonce-map-attach-pdf') ?>
<input type="hidden" id="map-attach-pdf-file" name="map-attach-pdf-file" value="<?= !empty($pdf_url) ? $pdf_url : '' ?>" />

<p style="background: #f5f5f5; padding: 0.35em 0.6em;"><span id="map-attach-pdf-file-label"><?= __('(no file)', 'monteverde') ?></span></p>

<a class="button" id="map-attach-pdf-action" href="#" id="post-preview"><?= __('Select PDF file', 'monteverde') ?></a>

<script type="text/javascript">
(function($) {

	var normal_ste = window.send_to_editor;

	var pdf_ste = function(html) {

		var pdf_url = $(html).attr('href');

		if (!pdf_url) {
			restore_ste_and_close();
			return;
		}

	    field.val(pdf_url);
	    refresh_pdf_label();

	    restore_ste_and_close();

	};

	var restore_ste_and_close = function() {

		tb_remove();
		window.send_to_editor = normal_ste;

	};

	var label = $('#map-attach-pdf-file-label'),
		field = $('#map-attach-pdf-file');

	var refresh_pdf_label = function() {

		var pdf_url = field.val();

		if (!pdf_url) {
			return;
		}

		label.text(pdf_url);

	};

	$('#map-attach-pdf-action').on('click', function(e) {

		e.preventDefault();
		window.send_to_editor = pdf_ste;
		tb_show('Upload PDF', 'media-upload.php?referer=map-attach-pdf&type=file&TB_iframe=true&post_id=0', false);

	});

	refresh_pdf_label();

})(jQuery);
</script>