<?php

add_action('pre_get_posts', function($query) {

	if (!$query->is_main_query() || !is_tax('place_type')) {
		return;
	}

	$meta_query = [];

	if (!empty($_GET['price_range']) && is_numeric($_GET['price_range'])) {

		$price_range = (int) $_GET['price_range'];

		if ($price_range > 0) {

			$meta_query[] = [
				'key' => 'price-range',
				'value' => $price_range,
				'compare' => '='
			];

		}

	}

	if (!empty($_GET['certifications'])) {

		if ($_GET['certifications'] == 'sustainable') {

			$meta_query[] = [
				'key' => 'sustainable-tourism',
				'value' => 0,
				'compare' => '>'
			];

		}

	}

	if (!empty($meta_query)) {

		$query->set('meta_query', $meta_query);

	}


});