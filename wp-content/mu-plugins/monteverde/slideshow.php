<?php defined('ABSPATH') or die;

// Post type

add_action('init', function() {

	register_post_type('slide', [
		'labels' => [
			'name' => _x('Slides', 'post type general name', 'monteverde'),
			'singular_name' => _x('Slide', 'post type singular name', 'monteverde'),
			'menu_name' => _x('Slides', 'admin menu', 'monteverde'),
			'name_admin_bar' => _x('Slide', 'add new on admin bar', 'monteverde'),
			'add_new' => _x('Add new', 'slide', 'monteverde'),
			'add_new_item' => __('Add New Slide', 'monteverde'),
			'new_item' => __('New Slide', 'monteverde'),
			'edit_item' => __('Edit Slide', 'monteverde'),
			'view_item' => __('View Slide', 'monteverde'),
			'all_items' => __('All Slides', 'monteverde'),
			'search_items' => __('Search Slides', 'monteverde'),
			'parent_item_color' => __('Parent Slides:', 'monteverde'),
			'not_found' => __('No slides found.', 'monteverde'),
			'not_found_in_trash' => __('No slides found in Trash', 'monteverde'),
		],
		'description' => __('Slides custom post type for Monteverde Engine.'),
		'public' => false,
		'show_ui' => true,
		'menu_position' => 20,
		'menu_icon' => 'dashicons-format-gallery',
		'capability_type' => 'page',
		'supports' => [
			'title',
			'editor',
			'thumbnail'
		]
	]);

	add_action('add_meta_boxes_slide', function() {

		remove_meta_box('postimagediv', 'slide', 'side');
	    add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'slide', 'normal', 'high');

	});

	add_filter('user_can_richedit', function($value) {

		global $post;

		if ($post->post_type === 'slide') return false;

		return $value;

	});

	VM_MetaBox::registerForCustomPostType('slide', [
		'id' => 'slideshow',
		'title' => __('Slideshow', 'monteverde'),
		'context' => 'side',
		'priority' => 'high',
		'fields' => [

			'active' => [
				'label' => __('Active', 'monteverde'),
				'type' => 'boolean'
			],

			'bound-to' => [
				'label' => __('Show in', 'monteverde'),
				'type' => 'enum-fn',
				'values' => function() {

					$output = [];

					$output[__('Pages', 'monteverde')] = [
						'@home' => __('Home Page', 'monteverde'),
						'@gallery' => __('Galleries listing', 'monteverde'),
					];

					/* Place types */

					$place_types_raw = get_terms('place_type', [
						'hide_empty' => 0
					]);

					$place_types = [];

					foreach ($place_types_raw as $place_type) {

						$place_types['place_type:' . $place_type->term_id] = $place_type->name;

					}

					$output[__('Place Types', 'monteverde')] = $place_types;

					/* Categories */

					$categories_raw = get_terms('category', [
						'hide_empty' => 0
					]);

					$categories = [];

					foreach ($categories_raw as $category) {

						$categories['category:' . $category->term_id] = $category->name;

					}

					$output[__('Categories', 'monteverde')] = $categories;

					/* Galleries */

					$galleries_raw = get_terms('gallery', [
						'hide_empty' => 0
					]);

					$galleries = [];

					foreach ($galleries_raw as $gallery) {

						$galleries['gallery:' . $gallery->term_id] = $gallery->name;

					}

					$output[__('Galleries', 'monteverde')] = $galleries;

					/* Bye */

					return $output;

				}
			],

			'position' => [
				'label' => __('Position', 'monteverde'),
				'type' => 'number',
				'required' => true,
				'default' => 0
			],

		]
	]);

	add_filter('manage_slide_posts_columns', function($columns) {

		return array_merge($columns, [
			'slide-active' => __('Active', 'monteverde'),
			'slide-bound-to-and-position' => __('Show in', 'monteverde')
		]);

	});

	add_filter('manage_slide_posts_custom_column', function($column) {

		global $post;

		switch ($column) {

			case 'slide-active':

				$active = get_post_meta($post->ID, 'active', true);
				echo '<input disabled type="checkbox" ' . ($active ? 'checked' : '') . ' />';
				break;

			case 'slide-bound-to-and-position':

				$bound_to = get_post_meta($post->ID, 'bound-to', true);
				$position = get_post_meta($post->ID, 'position', true);

				if (!$bound_to) {
					echo __('(loose)', 'monteverde');
					break;
				}

				if ($bound_to === '@home') {
					echo __('Home Page', 'monteverde');
				}
				elseif (strpos($bound_to, ':') !== false) {
					$term_args = explode(':', $bound_to, 2);
					$term = get_term_by('id', $term_args[1], $term_args[0]);
					echo $term->name;
				}
				else {
					echo __('(unknown)', 'monteverde');
				}

				echo '<br>&rarr; ' . __('Position', 'monteverde') . ': ' . ($position ?: '(default)');

				break;

		}

	});

});

