<?php

function mv_permalink_action($action, $post_id = null, array $extra_query = []) {

	$permalink = get_the_permalink($post_id);

	if (empty($permalink)) {
		return '';
	}

	$permalink .= strpos($permalink, '?') !== false ? '&' : '?';

	$query = array_merge($extra_query, [ 'mv_action' => $action ]);

	return $permalink . http_build_query($query);

}

function mv_get_cat_override($cat_id = null) {

	$cat = get_category($cat_id);
	$slug = $cat->slug;

	$page = get_page_by_path('category-' . $slug);

	if (empty($page)) {
		return null;
	}

	setup_postdata($page);

	$output = [];

	$output['id'] = get_the_ID();
	$output['title'] = get_the_title();
	$output['content'] = get_the_content();

	wp_reset_postdata();

	return $output;

}


function mv_server($match = null) {
	if (empty($_SERVER['SERVER_NAME'])) {
		return '';
	}

	if ($match !== null) {
		return stripos($_SERVER['SERVER_NAME'], $match) !== false;
	}

	return strtolower($_SERVER['SERVER_NAME']);
};
