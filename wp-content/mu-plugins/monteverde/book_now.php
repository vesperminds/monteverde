<?php

function mv_book_now($email, $data) {

	if (mv_server('staging.exploremonteverde.com'))
		$admin_email = 'vesperminds@gmail.com';
	elseif (mv_server('exploremonteverde.com'))
		$admin_email = ['camaramtv@gmail.com', 'vesperminds@gmail.com'];
	else
		$admin_email = 'edgebal@gmail.com';
		
	$place_email = get_post_meta((int) $data['place_id'], 'book-now-address', true);

	if (empty($place_email)) {
		$place_email = get_post_meta((int) $data['place_id'], 'email', true);
	}

	ob_start();
	include __DIR__ . '/helpers/book-now-user-email.php';
	$body = ob_get_clean();

	wp_mail($email, __('Booking confirmation', 'monteverde'), $body);

	ob_start();
	include __DIR__ . '/helpers/book-now-client-email.php';
	$body = ob_get_clean();

	wp_mail($admin_email, __('New booking: ' . $email, 'monteverde'), $body);

}
