<?php defined('ABSPATH') or die;
/**
 * Plugin Name: Monteverde
 * Description: Wordpress enhancements for Monteverde site
 * Version: 1.6
 * Author: Vesper Minds
 * Author URI: http://vesperminds.com/
 * Network: true
 */

/**
 * Custom types
 */
require_once __DIR__ . '/monteverde/custom-post-types.php';
require_once __DIR__ . '/monteverde/taxonomies.php';
require_once __DIR__ . '/monteverde/slideshow.php';
require_once __DIR__ . '/monteverde/misc.php';
require_once __DIR__ . '/monteverde/book_now.php';
require_once __DIR__ . '/monteverde/places-filter.php';

/**
 * Wordpress footer
 */
add_filter('admin_footer_text', function($text) {
	return $text . ' <i>Potenciado por <a href="http://vesperminds.com/" target="_blank">Vesper Minds</a><i>';
});

/**
 * Language
 */

add_action('plugins_loaded', function() {
	$loaded = load_muplugin_textdomain('monteverde', '/monteverde/lang');
});