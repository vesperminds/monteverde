<?php

$place_types = get_terms('place_type', [
	'orderby' => 'name',
	'order' => 'ASC',
	'fields' => 'all',
	'hierarchical' => true,
	'hide_empty' => false,
]);

?>
<div class="main-grid">
	<ul>
	<?php foreach ($place_types as $place_type): if (empty($place_type->parent)) continue; ?>
		<?php
			$image = z_taxonomy_image_url($place_type->term_id, 'thumbnail');
		?>
		<li style="<?= !empty($image) ? 'background-image: url(\'' . $image . '\');' : '' ?>">
			<a href="<?= mv_tax_link('place_type', $place_type->slug) ?>">
				<h3><?= $place_type->name ?></h3>
			</a>
		</li>
	<?php endforeach; ?>
	</ul>
</div>