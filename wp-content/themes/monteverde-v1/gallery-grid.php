<?php

	$galleries = get_terms('gallery', [
		'orderby' => 'name',
		'order' => 'ASC',
		'fields' => 'all',
		'hierarchical' => true,
		'hide_empty' => false
	]);

?>

<?php if (!empty($galleries)): ?>
<div class="main-grid-less">
	<ul>

		<?php foreach($galleries as $gallery): ?>
		<?php
			$image = z_taxonomy_image_url($gallery->term_id, 'thumbnail');
		?>
		<li style="<?= !empty($image) ? 'background-image: url(\'' . $image . '\');' : '' ?>">
			<a href="<?= mv_tax_link('gallery', $gallery->slug) ?>">
				<h3><?= $gallery->name ?></h3>
			</a>
		</li>
		<?php endforeach; ?>

	</ul>
</div>
<?php endif; ?>