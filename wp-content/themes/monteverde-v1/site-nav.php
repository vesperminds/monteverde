<nav class="main-nav">
	<ul>
	<?php

		wp_nav_menu([

			'theme_location' => 'main-menu',
			'container' => false,
			'items_wrap' => '%3$s',
			'depth' => 0,

		]);

	?>
	<?php get_template_part('social-networks-list'); ?>
	</ul>
</nav>
<?php

	$languages = icl_get_languages('skip_missing=1');
	$langActive = '';
	$langList = [];

	if (!empty($languages)) {

		foreach ($languages as $language) {

			if (!empty($language['active'])) {
				$langActive = strtoupper($language['language_code']);
			}

			$langList[$language['native_name']] = $language['url'];

		}

	}

?>
<?php if (!empty($langList)): ?>
<nav class="lang-nav">
	<ul>
		<li>
			<a href="#" onclick="return false;"><?= $langActive ?></a>
			<ul>
			<?php foreach ($langList as $lang => $url): ?>
				<li><a href="<?= $url ?>"><?= $lang ?></a></li>
			<?php endforeach; ?>
			</ul>
		</li>

	</ul>
</nav>
<?php endif; ?>