<?php

	$slidesObj = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => 'slide',
		'orderby' => 'meta_value_num',
		'meta_key' => 'position',
		'order' => 'ASC',
		'meta_query' => [
			[
				'key' => 'active',
				'value' => '1'
			],
			[
				'key' => 'bound-to',
				'value' => defined('CUSTOM_SLIDESHOW_BINDING') ?
					CUSTOM_SLIDESHOW_BINDING : '@home'
			]
		]
	]);

	$slides = [];
	$total_slides = 0;

	if ($slidesObj->have_posts()) {

		while ($slidesObj->have_posts()) {

			$slidesObj->the_post();
			$total_slides++;

			$slides[] = [
				'id' => get_the_ID(),
				'text' => do_shortcode(get_the_content()),
				'images' => mvt_get_hero_images()
			];

		}

		wp_reset_postdata();

	}
	else {
		return;
	}

	$selector_size_px = 12;
	$selector_margin_px = 8;

	$selector_step_px = $selector_size_px + $selector_margin_px;
	$selector_total_px = ($total_slides * $selector_step_px) - $selector_margin_px;
	$selector_initial_px = ($selector_total_px / 2) - $selector_total_px;

?>

<style type="text/css">

.slides-container {
	width: <?= ($total_slides + 1) * 100 ?>%;
}

.slide {
	width: <?= round((1 / ($total_slides + 1)) * 100, 1) ?>%;
}

<?php $i = -1; foreach ($slides as $slide): $i++; $id = $slide['id']; ?>

#slide-selector--<?= $id ?> {
	margin-left: <?= $selector_initial_px + ($selector_step_px * $i) ?>px;
}

#slide-control--<?= $id ?>:checked ~ .slides-container {
	margin-left: <?= -($i * 100) ?>%;
}

#slide-control--<?= $id ?>:checked + #slide-selector--<?= $id ?> {
	opacity: 1;
}

#slide--<?= $id ?> {
	background-image: url('<?= $slide['images']['desktop'] ?>');
}

<?php if (!empty($slide['images']['mobile'])): ?>
@media only screen and (max-width: 960px) {
	#slide--<?= $id ?> {
		background-image: url('<?= $slide['images']['mobile'] ?>');
	}
}
<?php endif; ?>

<?php endforeach; ?>
</style>

<?php $checked = false; foreach ($slides as $slide): ?>
	<input <?php if (!$checked) { echo 'checked="checked"'; $checked = true; } ?> type="radio" name="slide" class="slide-control" id="slide-control--<?= $slide['id'] ?>" />
	<label for="slide-control--<?= $slide['id'] ?>" id="slide-selector--<?= $slide['id'] ?>" class="slide-selector"></label>
<?php endforeach; ?>

<div class="slides-container">

	<?php foreach ($slides as $slide): ?>
	<div class="slide" id="slide--<?= $slide['id']?>">
		<div class="slide-text"><?= $slide['text'] ?></div>
	</div>
	<?php endforeach; ?>

</div>