<?php

	global $wp_query;
	
	$pag_pages = isset($wp_query->max_num_pages) ? $wp_query->max_num_pages : 0;
	$pag_page = get_query_var('paged') ?: 1;

	$optvalsel = function($field, $val = '') {

		$output = 'value="' . $val . '"';
		$cur = !empty($_GET[$field]) ? $_GET[$field] : null;

		if ($cur == $val) {
			$output .= ' selected="selected"';
		}

		return $output;

	}

?>
<div class="inner-info-content">

	<h2><?= single_cat_title( '', false ) ?></h2>
	<p><?= get_the_archive_description() ?></p>

	<form class="place-filters">
		<label><?= __('View by', 'monteverde-th') ?>:</label>
		<div class="break"></div>
		<select name="price_range">
			<option value=""><?= __('All price ranges', 'monteverde-th') ?></option>
			<option <?= $optvalsel('price_range', '1') ?> >$</option>
			<option <?= $optvalsel('price_range', '2') ?> >$$</option>
			<option <?= $optvalsel('price_range', '3') ?> >$$$</option>
			<option <?= $optvalsel('price_range', '4') ?> >$$$$</option>
		</select>
		<div class="break"></div>
		<select name="certifications">
			<option value=""><?= __('Any or no certifications', 'monteverde-th') ?></option>
			<option <?= $optvalsel('certifications', 'sustainable') ?> ><?= __('Sustainable Tourism', 'monteverde-th') ?></option>
		</select>
		<div class="break"></div>
		<button><?= __('Go', 'monteverde-th') ?></button>
	</form>

	<?php if (have_posts()): ?>
	<ul class="catalog-grid clear">
		<!-- the_post(); -->
		<?php $i = 1; while (have_posts()): the_post(); $i++ ?>
		<?php

			$image = null;
			$thumb = null;
			$cover = false;

			$re = "/\\[gallery[^\\]]+ids=\\\"([0-9]+)/i";
			$str = (string) get_the_content();
			$matches = null;

			preg_match($re, $str, $matches);

			if (!empty($matches[1])) {
				$image = wp_get_attachment_image_src((int) $matches[1], 'monteverde-post-thumbnail');
				$cover = true;
			}

			if (empty($image)) {
				$thumbnail_id = get_post_thumbnail_id();
				$image = wp_get_attachment_image_src($thumbnail_id, 'monteverde-post-thumbnail-cover');
				$cover = false;
			}

			if (!empty($image)) {
				$thumb = $image[0];
			}

			$features = get_post_meta(get_the_ID(), 'features', true);

			if (!empty($features)) {
				$features = trim(preg_replace("/(\\r?\\n){1,}/", "\n", $features));
				$features = explode("\n", $features);
			}
			else {
				$features = [];
			}

		?>
		<li class="place-cell">
			<div style="<?=
				!empty($thumb) ? 'background-image: url(\'' . $image[0] . '\');' : ''
				?> <?=
				!empty($cover) ? 'background-size: cover;' : ''
				?>">
				<!-- <a class="see-more-label" href="<?php the_permalink() ?>">
					<h4><?= __('See More', 'monteverde-th') ?></h4>
					<img src="<?= mvt_url('/img/triangle-btn.png') ?>" alt="" class="triangle">
				</a> -->
				<a class="see-more-area" href="<?php the_permalink() ?>">
					<span class="see-more-legend">
						<span>
							<img class="see-more-arrow" src="<?= mvt_url('/img/triangle-btn.png') ?>" alt="" class="triangle">
							<?= __('See More', 'monteverde-th') ?>
						</span>
					</span>
				</a>
			</div>

			<h3><?php the_title(); ?></h3>
			<p>
				<?= implode(', ', $features) ?>
			</p>
			<em class="clear"></em>
		</li>
		<?php endwhile; ?>

	</ul>

	<div class="pagination">
		<?php previous_posts_link(__('&laquo; Prev', 'monteverde-th')); ?>
		<span><?= sprintf(__('Page %s of %s', 'monteverde-th'), $pag_page, $pag_pages) ?></span>
		<?php next_posts_link(__('Next &raquo;', 'monteverde-th')); ?>
	</div>
	<?php else: ?>
	<div class="not-found-criteria">
		<p><?= __('There are no places matching your criteria. Please broaden your search and try again.') ?></p>
	</div>
	<?php endif; ?>

</div>
