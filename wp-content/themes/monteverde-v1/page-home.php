<?php

$posts_per_page = 5;
$paged = !empty($_GET['paged']) ? absint($_GET['paged']) : 1;
if ($paged < 1) $paged = 1;

$args = [
	'post_type' => 'post',
	'posts_per_page' => $posts_per_page,
	'paged' => $paged
];

$HomePostListData = [
	'query' => new WP_Query($args),
	'paged' => $paged
];

$HomePostListData['end'] = $HomePostListData['query']->post_count < $posts_per_page;

if (!empty($_GET['ajax'])) {

	header('Content-type: application/json');

	if ($HomePostListData->post_count === 0) {
		echo json_encode([ 'count' => 0, 'html' => '', 'end' => true ]);
		exit;
	}

	$end = $HomePostListData['end'];

	ob_start();
	include locate_template('home-post-list.php');
	$html = ob_get_clean();

	echo json_encode([
		'count' => $HomePostListData['query']->post_count,
		'html' => $html,
		'end' => $HomePostListData['end']
	]);

	exit;

}

mvt_set_page_title(_x('Welcome', 'welcome title on home page', 'monteverde-th'));

?>

<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<header class="main-header main-header-home">

	<div class="em-logo"></div>
	<h1>Explore Monteverde</h1>

	<?php get_template_part('site-nav'); ?>

	<?php get_template_part('slideshow'); ?>

</header>

<div class="main-container">

	<?php get_template_part('categories-table'); ?>

	<?php get_template_part('subcategories-grid'); ?>

	<?php include locate_template('home-post-list.php'); ?>

</div>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>
