<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<?php
	$images = mvt_get_hero_images();
?>
<?php if (!empty($images['desktop'])): ?>
<style type="text/css">
	.main-header-gallery-<?php the_ID(); ?> {
		background-image: url('<?= $images['desktop'] ?>') !important;
	}

	<?php if (!empty($images['mobile'])): ?>
	@media only screen and (max-width: 40em) {
		.main-header-gallery-<?php the_ID(); ?> {
			background-image: url('<?= $images['mobile'] ?>') !important;
		}
	}
	<?php endif; ?>
</style>
<?php endif; ?>

<header class="main-header main-header-gallery-<?php the_ID(); ?>">

	<?php get_template_part('site-nav'); ?>

	<div class="header-label category-header-label">
		<h1>
			<?php the_title(); ?><br>
			<strong>MONTEVERDE</strong>
		</h1>
		<p>
			<?= strip_tags(get_the_content()) ?>
		</p>
	</div>

	<?php
		define('CUSTOM_SLIDESHOW_BINDING', '@gallery');
		get_template_part('slideshow');
	?>

</header>

<div class="main-container">

	<?php get_template_part('gallery-grid') ?>
	<?php get_template_part('last-post-list'); ?>

</div>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>