<?php

	$pt = get_queried_object();
	$pt_id = $pt->term_id;
	$parent_pt = null;
	$parent_pt_id = !empty(get_queried_object()->parent) ? get_queried_object()->parent : null;

	if (!empty($parent_pt_id)) {
		$parent_pt = get_term_by('id', $parent_pt_id, 'gallery');
	}

?>
<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<?php

	$image = z_taxonomy_image_url($pt_id, 'monteverde-hero-image');
	$mobile_image = z_taxonomy_image_url($pt_id, 'monteverde-mobile-hero-image');

?>
<?php if (!empty($image)): ?>
<style type="text/css">
	.main-header-gallery-<?= $pt_id ?> {
		background-image: url('<?= $image ?>') !important;
	}

	<?php if (!empty($mobile_image)): ?>
	@media only screen and (max-width: 40em) {
		.main-header-gallery-<?= $pt_id ?> {
			background-image: url('<?= $mobile_image ?>') !important;
		}
	}
	<?php endif; ?>
</style>
<?php endif; ?>

<header class="main-header main-header-gallery-<?= $pt_id ?>">

	<?php get_template_part('site-nav'); ?>

	<div class="header-label category-header-label">
		<h1>
			<?= !empty($parent_pt->name) ? $parent_pt->name : single_cat_title( '', false ) ?><br>
			<strong>MONTEVERDE</strong>
		</h1>
		<p>
			<?= !empty($parent_pt->description) ? $parent_pt->description : get_the_archive_description() ?>
		</p>
	</div>

	<?php
		define('CUSTOM_SLIDESHOW_BINDING', 'gallery:' . $cat_id);
		get_template_part('slideshow');
	?>

</header>

<div class="main-container">

	<?php get_template_part('photo-list') ?>
	<?php get_template_part('last-post-list'); ?>

</div>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>