<?php

if (!have_posts()) {
	die();
}

the_post();

$url = wp_get_attachment_url($post->ID);

if (empty($url)) {
	die;
}

wp_redirect($url); exit;