<?php defined('ABSPATH') or die; ?>
<footer class="main-footer">

	<div class="us">
		<h4><?= _x('CONNECT WITH US', 'footer', 'monteverde-th') ?></h4>
		<p><?= _x('Join our online community and stay up to date.', 'footer', 'monteverde-th') ?></p>
		<ul class="social-channels">
			<?php get_template_part('social-networks-list'); ?>
		</ul>
		<ul>
			<li>
				<p>
					<?= _x('Phone', 'footer', 'monteverde-th') ?>: (506) 2645 - 6565
				</p>
			</li>
			<li>
				<p>
					<?= _x('Fax', 'footer', 'monteverde-th') ?>: (506) 2645 - 6464
				</p>
			</li>
			<li>
				<p>
					<?= _x('Email', 'footer', 'monteverde-th') ?>: <a href="mailto:info@exploremonteverde.com?subject=Contact">info@exploremonteverde.com</a>
				</p>
			</li>
		</ul>
	</div>

	<div class="newsletter" ng-controller="newsletterCtrl">

		<h4><?= _x('JOIN OUR NEWSLETTER', 'footer', 'monteverde-th') ?></h4>
		<p><?= _x('Join to our newsletter to get free content delivered automatically each time we publish.', 'footer', 'monteverde-th') ?></p>

		<form class="newsletter-form" name="newsletterForm" method="post"
		ng-submit="subscribe()"><!-- novalidate -->

			<div class="newsletter-field">
				<label for=""><?= _x('Your name', 'footer', 'monteverde-th') ?></label>
				<input id="fullname" type="text" name="fullname" ng-model="subscriber.fullname" required>
			</div>
			<div class="newsletter-field">
				<label for=""><?= _x('Your email address', 'footer', 'monteverde-th') ?></label>
				<input id="email" type="email" name="email" ng-model="subscriber.email" required>
			</div>

			<div class="newsletter-field newsletter-submit">
				<button id="sign-up" type="submit"><?= _x('Sign up', 'footer', 'monteverde-th') ?></button>
			</div>

		</form>

		<div class="newsletter-thanks" style="display: none;">
			<p><b>
				<?= __('Thank you for signing up our newsletter. Your email address will be kept confidential and will not be shared with anyone else.', 'monteverde-th') ?>
			</b></p>
		</div>

	</div>

	<div class="back-to-top-mobile">

		<a href="#" id="back-to-top"><i></i> Back to top</a>

	</div>

</footer>

<div class="footer-minilinks">
	<a href="<?= get_site_url(null, _x('/en/policies', 'URL de politicas', 'monteverde-th')) ?>"><?= __('Cancellation Policies', 'monteverde-th') ?></a>
</div>
