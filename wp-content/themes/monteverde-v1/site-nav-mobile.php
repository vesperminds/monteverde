<ul class="mobile-menu">
<?php

	wp_nav_menu([

		'theme_location' => 'main-menu',
		'container' => false,
		'items_wrap' => '%3$s',
		'depth' => 0,

	]);

?>

<?php

	$languages = icl_get_languages('skip_missing=1');
	$langList = [];

	if (!empty($languages)) {

		foreach ($languages as $language) {
			if (!empty($language['active'])) continue;
			$langList[$language['native_name']] = $language['url'];
		}

	}

?>
<?php if (!empty($langList)): ?>
<?php foreach ($langList as $lang => $url): ?>
	<li><a href="<?= $url ?>"><?= $lang ?></a></li>
<?php endforeach; ?>
<?php endif; ?>

	<li>
		<ul>
			<?php get_template_part('social-networks-list'); ?>
		</ul>
	</li>

</ul>

<input id="nav-trigger" class="nav-trigger" type="checkbox">
<label for="nav-trigger"></label>