<ul class="pdf-files clear">

	<?php foreach ($maps as $map): ?>
	<li><a href="<?= $map['permalink'] ?>">
		<div class="map-thumb" style="background-image: url('<?= $map['thumb'] ?>');"></div>
		<footer>
			<h3><?= $map['title'] ?></h3>
			<p><?= __('Map Info (PDF)', 'monteverde-th') ?></p>
		</footer>
	</a></li>
	<?php endforeach; ?>

</ul>
