<br>
<div class="info-grid">

	<?php

		$image_objs = [];

		foreach ($image_ids as $image_id) {
			$image_objs[$image_id] = wp_get_attachment_image_src($image_id, 'monteverde-post-thumbnail');
		}

	?>

	<div style="background-image: url('<?= $image_objs[$image_ids[0]][0] ?>');" class="gallery-current-image"></div>

	<ul>

		<?php foreach ($image_ids as $image_id): ?>
		<li style="background-image: url('<?= $image_objs[$image_id][0] ?>');" class="gallery-image-selector"
			data-image="<?= $image_objs[$image_id][0] ?>">
			<a href="#"></a>
		</li>
		<?php endforeach; ?>

	</ul>

	<section class="side-info">

		<header class="info-square-features">
			<h3><?= __('Features', 'monteverde-th') ?></h3>
		</header>

		<section class="features">
			<p>
				<?= implode('<br><br>', $features) ?>
			</p>
		</section>

	</section>

</div>