<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<?php while (have_posts()): the_post() ?>

<?php

	$thumb_id = get_post_thumbnail_id();
	$thumb = wp_get_attachment_image_src($thumb_id, 'monteverde-post-thumbnail-cover');

	$galleries = wp_get_post_terms(get_the_ID(), 'gallery', [ 'fields' => 'all' ]);

	if (!empty($galleries[0])) {
		$gallery = $galleries[0];
	}

	if (!empty($galleries[0])) {
		$image = z_taxonomy_image_url($gallery->term_id, 'monteverde-hero-image');
		$mobile_image = z_taxonomy_image_url($gallery->term_id, 'monteverde-mobile-hero-image');
	}

?>
<?php if (!empty($image)): ?>
<style type="text/css">

	<?php if (!empty($image)): ?>
	.main-header-<?php the_ID(); ?> {
		background-image: url('<?= $image ?>') !important;
	}

	@media only screen and (max-width: 40em) {
		.main-header-<?php the_ID(); ?> {
			background-image: url('<?= $mobile_image ?>') !important;
		}
	}
	<?php endif; ?>
</style>
<?php endif; ?>

<header class="main-header main-header-<?php the_ID(); ?>">

	<?php get_template_part('site-nav'); ?>

	<div class="header-label post-header-label">
		<h1>
			<?= $gallery->name ?><br>
			<strong>MONTEVERDE</strong>
		</h1>
		<p><?= $gallery->description ?></p>
	</div>

</header>

<div class="main-container">

	<div class="inner-info-content">

		<h2>
			<?php the_title(); ?>
		</h2>

		<?php

			$thumb_id = get_post_thumbnail_id();
			$content = wp_get_attachment_image_src($thumb_id, 'monteverde-gallery-image');

		?>

		<?php if (!empty($content)): ?>
		<p>
			<img class="photo" src="<?= $content[0] ?>" />
		</p>
		<?php endif; ?>

		<a href="#" onclick="history.go(-1); return false;" class="btn-seemore btn-back"><?= __('Back', 'monteverde-th') ?></a>

	</div>

</div>

<?php break; endwhile; ?>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>