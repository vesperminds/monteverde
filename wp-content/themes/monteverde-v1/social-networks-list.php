<li class="social-icon-wrapper">
	<a class="social-icon facebook" href="https://www.facebook.com/monteverdethecloudforest?ref=hl" target="_blank" title="<?= __('Follow us on Facebook', 'monteverde-th') ?>"><i></i></a>
</li>
<li class="social-icon-wrapper">
	<a class="social-icon twitter" href="https://twitter.com/xpmonteverde" target="_blank" title="<?= __('Follow us on Twitter', 'monteverde-th') ?>"><i></i></a>
</li>
<li class="social-icon-wrapper">
	<a class="social-icon gplus" href="https://plus.google.com/u/0/b/114076164023856249828/114076164023856249828" target="_blank" title="<?= __('Follow us on Google+', 'monteverde-th') ?>"><i></i></a>
</li>
<li class="social-icon-wrapper">
	<a class="social-icon pinterest" href="https://www.pinterest.com/xpmonteverde" target="_blank" title="<?= __('Follow us on Pinterest', 'monteverde-th') ?>"><i></i></a>
</li>
<li class="social-icon-wrapper">
	<a class="social-icon instagram" href="https://instagram.com/xpmonteverde/" target="_blank" title="<?= __('Follow us on Instagram', 'monteverde-th') ?>"><i></i></a>
</li>