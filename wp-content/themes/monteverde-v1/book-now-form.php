<?php

	$endpoint = get_the_permalink();
	$endpoint .= strpos($endpoint, '?') !== false ? '&' : '?';
	$endpoint .= 'book_now=1'

?>
<form action="<?= mv_permalink_action('book-now-place') ?>"
method="post" class="book-now-form" id="book-now-form" style="display: none;">

	<div class="open-form">

		<input type="hidden" name="endpoint" value="<?= $endpoint ?>">
		<input type="hidden" name="place_id" value="<?= get_the_ID(); ?>">
		<input type="hidden" name="place_name" value="<?= get_the_title(); ?>">

		<div class="fieldset">
			<p><?= __(
				'Please fill the following fields in order to process your reservation. After completing the requested info, you will receive a confirmation email.',
				'monteverde-th'
			) ?></p>
			<p><?= sprintf(__(
				'You are booking now to visit %s',
				'monteverde-th'
			), '<b>' . get_the_title() . '</b>') ?></p>
			<p class="small-note"><?= __(
				'* Required fields',
				'monteverde-th'
			) ?></p>
		</div>

		<div class="fieldset">
			<div class="field">
				<label for="book_now_first_name">* <?= __('First Name', 'monteverde-th') ?></label>
				<input type="text" id="book_now_first_name" name="first_name" required />
			</div>
			<div class="field">
				<label for="book_now_last_name">* <?= __('Last Name', 'monteverde-th') ?></label>
				<input type="text" id="book_now_last_name" name="last_name" required />
			</div>
		</div>
		<div class="fieldset">
			<div class="field">
				<label for="book_now_email">* <?= __('Email', 'monteverde-th') ?></label>
				<input type="email" id="book_now_email" name="email" required />
			</div>
			<div class="field dual-field">
				<label for="book_now_adults">* <?= __('Adults', 'monteverde-th') ?></label>
				<input type="number" id="book_now_adults" name="adults" min="1" max="99" maxlength="2" required>
				<label class="last-label" for="book_now_children">* <?= __('Children', 'monteverde-th') ?></label>
				<input type="number" id="book_now_children" name="children" min="0" max="99" maxlength="2" required>
			</div>
		</div>
		<div class="fieldset">
			<div class="field">
				<label for="book_now_phone"><?= __('Phone Number', 'monteverde-th') ?></label>
				<input type="text" id="book_now_phone" name="phone" />
			</div>
			<div class="field">
				<label for="book_now_arrival_date">* <?= __('Arrival Date', 'monteverde-th') ?></label>
				<input type="text" id="book_now_arrival_date" name="arrival_date" required />
			</div>
		</div>
		<div class="fieldset" id="children_ages_fieldset" style="display: none;">
			<div class="lone-field">
				<label>
					<?= __('Children\'s ages:', 'monteverde-th') ?>
				</label><br>
				<div class="select-stream" id="children_ages_container">

				</div>
			</div>
		</div>
		<div class="fieldset">
			<div class="lone-field">
				<label for="book_now_details">
					<?= __('Please enter any special requests you would like to make:', 'monteverde-th') ?>
				</label><br>
				<textarea id="book_now_details" name="details"></textarea>
			</div>
		</div>
		<div class="fieldset">
			<div class="lone-field">
				<a href="#" class="btn-book-now-ok btn-seemore">
					<?= __('Send booking', 'monteverde-th') ?>
				</a>
				&nbsp;
				<a href="#" class="btn-book-now-cancel btn-seemore">
					<?= __('Cancel', 'monteverde-th') ?>
				</a>
			</div>
		</div>

	</div>
	<div class="closed-form" style="display: none;">

		<div class="fieldset">
			<p><?= sprintf(__(
				'Thanks for registering to visit %s. We\'ve received all your information. You will receive a confirmation email in a few minutes.',
				'monteverde-th'
			), '<b>' . get_the_title() . '</b>') ?></p>
			<p><?= __(
				'We have much to offer you! Return to the home page and discover all the places that are waiting for you.',
				'monteverde-th'
			) ?></p>
			<p>
				<br>
				<a href="<?= get_site_url(null, '/') ?>" class="btn-seemore">
					<?= __('Go to Home', 'monteverde-th') ?>
				</a>
			</p>
		</div>

	</div>

</form>
