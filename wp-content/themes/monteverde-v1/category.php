<?php

	$cat_id = (int) get_query_var('cat');
	$override = mv_get_cat_override($cat_id);

?>
<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<?php

	$image = z_taxonomy_image_url($cat_id, 'monteverde-hero-image');
	$mobile_image = z_taxonomy_image_url($cat_id, 'monteverde-mobile-hero-image');

?>
<?php if (!empty($image)): ?>
<style type="text/css">
	.main-header-cat-<?= $cat_id ?> {
		background-image: url('<?= $image ?>') !important;
	}

	<?php if (!empty($mobile_image)): ?>
	@media only screen and (max-width: 40em) {
		.main-header-cat-<?= $cat_id ?> {
			background-image: url('<?= $mobile_image ?>') !important;
		}
	}
	<?php endif; ?>
</style>
<?php endif; ?>

<header class="main-header main-header-cat-<?= $cat_id ?>">

	<?php get_template_part('site-nav'); ?>

	<div class="header-label category-header-label">
		<h1>
			<?= single_cat_title( '', false ) ?><br>
			<strong>MONTEVERDE</strong>
		</h1>
		<p>
			<?php the_archive_description() ?>
		</p>
	</div>

	<?php
		define('CUSTOM_SLIDESHOW_BINDING', 'category:' . $cat_id);
		get_template_part('slideshow');
	?>

</header>

<div class="main-container">

	<div class="inner-info-content">
	<?php if (!empty($override)): ?>

		<div class="inner-info-article">

			<?= apply_filters('the_content', $override['content']) ?>

			<br>
			<br>
			<br>

			<?php get_template_part('category-grid') ?>

			<?php get_template_part('last-post-list'); ?>

		</div>

	<?php elseif (have_posts()): ?>
	<?php while (have_posts()): the_post(); ?>

		<div class="inner-info-article">

			<h3><?php the_title(); ?></h3>
			<h4><?php the_date(); ?></h4>

			<p>
				<?= strip_tags(get_the_excerpt()) ?>
			</p>

			<p>
				<a href="<?php the_permalink() ?>" class="btn-seemore"><?= __('Read More', 'monteverde-th') ?></a>
			</p>

		</div>

	<?php endwhile; ?>
	<?php elseif (empty($override)): ?>

		<h3><?= _x('Oops!', 'not found title', 'monteverde-th') ?></h3>

		<p>
			<?= __('There\'s no articles yet! Please come back later.', 'monteverde-th') ?>
		</p>

		<p style="margin-top: 3em;">
			<a href="#" onclick="history.go(-1); return false;" class="btn-seemore btn-back"><?= __('Back', 'monteverde-th') ?></a>
		</p>

	<?php endif; ?>

	</div>

</div>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>