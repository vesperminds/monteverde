<?php
	$pt = get_queried_object();
	$pt_id = $pt->term_id;

	$place_types = get_terms('place_type', [
		'orderby' => 'name',
		'order' => 'ASC',
		'fields' => 'all',
		'hierarchical' => true,
		'hide_empty' => false,
		'child_of' => $pt_id
	]);

?>

<?php if (!empty($place_types)): ?>
<div class="main-grid-less">
	<ul>

		<?php foreach($place_types as $place_type): ?>
		<?php
			$image = z_taxonomy_image_url($place_type->term_id, 'thumbnail');
		?>
		<li style="<?= !empty($image) ? 'background-image: url(\'' . $image . '\');' : '' ?>">
			<a href="<?= mv_tax_link('place_type', $place_type->slug) ?>">
				<h3><?= $place_type->name ?></h3>
			</a>
		</li>
		<?php endforeach; ?>

	</ul>
</div>
<?php endif; ?>