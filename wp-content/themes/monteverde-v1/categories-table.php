<?php

	$term_ids = [
		[ 'place_type', 'eat-and-drink'],
		[ 'place_type', 'stay'],
		[ 'place_type', 'see-and-do'],
		[ 'category', 'travel-tips']
	];

	$terms = mv_tax_list($term_ids)

?>

<div class="content-categories">

	<ul>

		<?php foreach ($terms as $term): ?>
		<li class="index-categories" >
			<div style="background-image: url('<?= $term['image'] ?>');"></div>
			<h3><?= $term['name'] ?></h3>
			<p>
				<?= $term['description'] ?>
			</p>
			<a href="<?= $term['link'] ?>" class="btn-seemore"><?= __('Read More', 'monteverde-th') ?></a>
		</li>
		<?php endforeach; ?>

	</ul>

</div>
<!-- mv_tax_link('place_type', 'eat-and-drink') -->
