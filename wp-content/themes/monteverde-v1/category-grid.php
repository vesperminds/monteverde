<?php
	$pt = get_queried_object();
	$pt_id = $pt->term_id;

	$categories = get_terms('category', [
		'orderby' => 'name',
		'order' => 'ASC',
		'fields' => 'all',
		'hierarchical' => true,
		'hide_empty' => false,
		'child_of' => $pt_id
	]);

?>

<?php if (!empty($categories)): ?>
<div class="main-grid-less">
	<ul>

		<?php foreach($categories as $category): ?>
		<?php
			$image = z_taxonomy_image_url($category->term_id, 'thumbnail');
		?>
		<li style="<?= !empty($image) ? 'background-image: url(\'' . $image . '\');' : '' ?>">
			<a href="<?= mv_tax_link('category', $category->slug) ?>">
				<h3><?= $category->name ?></h3>
			</a>
		</li>
		<?php endforeach; ?>

	</ul>
</div>
<?php endif; ?>