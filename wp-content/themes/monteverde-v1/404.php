<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<header class="main-header">

	<h1><?= _x('Explore Monteverde', 'home page title', 'monteverde-th') ?></h1>

	<?php get_template_part('site-nav'); ?>

	<?php get_template_part('slideshow'); ?>

</header>

<div class="main-container">

</div>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>