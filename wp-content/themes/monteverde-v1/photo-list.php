<div class="inner-info-content">

	<h2><?= single_cat_title( '', false ) ?></h2>
	<p><?= get_the_archive_description() ?></p>

	<?php if (have_posts()): ?>
	<ul class="catalog-grid clear">
		<!-- the_post(); -->
		<?php $i = 1; while (have_posts()): the_post(); $i++ ?>
		<?php

			$thumbnail_id = get_post_thumbnail_id();
			$image = wp_get_attachment_image_src($thumbnail_id, 'monteverde-post-thumbnail');

		?>
		<li class="place-cell">
			<div class="cg-cover" style="<?= !empty($image) ? 'background-image: url(\'' . $image[0] . '\');' : '' ?>">
				<a class="see-more-area" href="<?php the_permalink() ?>">
					<span class="see-more-legend">
						<span>
							<img class="see-more-arrow" src="<?= mvt_url('/img/triangle-btn.png') ?>" alt="" class="triangle">
							<?= __('View', 'monteverde-th') ?>
						</span>
					</span>
				</a>
			</div>
			<em class="clear"></em>
		</li>
		<?php endwhile; ?>

	</ul>

	<div class="pagination">
		<?php posts_nav_link( ' &#183; ', __('&laquo; Prev', 'monteverde-th'), __('&raquo; Next', 'monteverde-th') ); ?>
	</div>
	<?php endif; ?>

</div>