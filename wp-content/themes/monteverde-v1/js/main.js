/* Mobile menu */

$(function() {

	var button = $('label[for="nav-trigger"]'),
		el = $('#nav-trigger'),
		menu = $('.mobile-menu'),
		body = $('body:first');

	if (button.length === 0) {
		return;
	}

	button.click(function(ev) {

		ev.stopPropagation();

		if (!el.is(':checked')) {
			menu.addClass('mobile-menu-open');
		}
		else {
			menu.removeClass('mobile-menu-open');
		}

	});

	el.click(function(ev) {

		ev.stopPropagation();

	});

	menu.click(function(ev) {

		ev.stopPropagation();

	});

	body.click(function() {

		if (!menu.hasClass('mobile-menu-open')) {
			return;
		}

		menu.removeClass('mobile-menu-open');
		el.prop('checked', false);

	});

	menu.removeClass('mobile-menu-open');
	el.prop('checked', false);

});

/* Gallery */

$(function() {

	var curr = $('.gallery-current-image');

	if (curr.length === 0) {
		return;
	}

	var sel = $('.gallery-image-selector');

	sel.on('click', function() {

		var img = $(this).data('image');

		if (!img) {
			return;
		}

		curr.css({
			'background-image': "url('" + img + "')"
		});

	});

});

/* Back to top */

$(function() {

	var btt = $('#back-to-top'),
		body = $('body:first');

	if (btt.length === 0) {
		return;
	}

	btt.click(function(ev) {

		ev.preventDefault();

		body.stop().animate({
			scrollTop: 0
		}, 500);

	});

});

/* TEMPORARY: Newsletter form */

$(function() {

	var form = $('form.newsletter-form'),
		thanks = $('.newsletter-thanks');

	if (form.length === 0) {
		return;
	}

	var name = form.find('[name="fullname"]').first(),
		email = form.find('[name="email"]').first();

	name.on('click focus blur', function() {
		$(this).removeClass('v-invalid');
	});

	email.on('click focus blur', function() {
		$(this).removeClass('v-invalid');
	});

	form.find('[type="submit"]').click(function(ev) {

		ev.preventDefault();

		var failed = false;

		if (!name.val()) {
			name.addClass('v-invalid');
			failed = true;
		}

		if (!email.val() || email.val().indexOf('@') < 0 || email.val().indexOf('.') < 0) {
			email.addClass('v-invalid');
			failed = true;
		}

		if (failed) return;

		var data = {
			'action': 'add',
			'name': name.val(),
			'email': email.val()
		}

		$.ajax({
			type: 'POST',
			url: _BASEPATH + '/_OLD_/subscribe.php',
			data: data
		}).then(function() {
			thanks.fadeIn();
		});

		form.hide();

	});

});

/* Book now */

$(function() {

	var button = $('.btn-book-now'),
		form = $('#book-now-form'),
		openForm = form.find('.open-form'),
		closedForm = form.find('.closed-form'),
		ok = $('.btn-book-now-ok'),
		cancel = $('.btn-book-now-cancel'),
		endpoint = form.find('[name="endpoint"]');

	if (!button.length) {
		return;
	}

	var validate = function(form) {

		var valid = true;

		var invalid = function(field) {
			valid = false;
			$(field).data('v-invalid', true).addClass('field-error');
		};

		form.find('input, select, textarea').removeClass('field-error').each(function() {
			$(this).data('v-invalid', false);
		});

		/* emptyness */

		form.find('input[required], select[required], textarea[required]').each(function() {

			var field = $(this);

			if ($(this).data('v-invalid')) return;

			if (!field.val().length) {
				invalid(this);
			}

		});

		/* email */

		form.find('input[type=email]').each(function() {

			var field = $(this),
				val = field.val();

			if ($(this).data('v-invalid')) return;

			if (!val) return;

			if (val.indexOf('@') < 0 || val.indexOf('.') < 0) {
				invalid(this);
			}

		});

		/* end */

		return valid;

	};

	button.on('click', function(ev) {

		ev.preventDefault();

		button.hide();
		form.slideDown(function() {
			form.find('select, input, textarea').first().focus();
		});

	});

	cancel.on('click', function(ev) {

		ev.preventDefault();

		form.slideUp();
		button.fadeIn();

	});

	ok.on('click', function(ev) {

		ev.preventDefault();

		var valid = validate(form);

		if (!valid) {
			return;
		}

		ok.hide();
		cancel.hide();

		$.ajax({
			url: endpoint.val(),
			type: 'post',
			data: form.serialize()
		}).then(function() {

			openForm.hide();
			closedForm.show();

		});

	});

	var caCont = $('#children_ages_container'),
		caFs = $('#children_ages_fieldset');

	$('#book_now_children').on('change blur', function(ev) {

		var num = parseInt($(this).val(), 10),
			sels = caCont.find('[name="children_age[]"]'),
			cur = sels.length,
			el, i, age;

		if ($(this).val() === '') num = 0;

		if (num > 12) num = 12;
		if (num < 0) num = 0;

		if (num < 1) {
			caFs.slideUp();
		}
		else {
			caFs.slideDown();
		}

		if (num > cur) {
			for (i = cur; i < num; i++) {
				el = $('<select></select>');
				el.attr({
					name: 'children_age[]',
					id: 'children_age_' + i
				}).addClass('children-age').appendTo(caCont);

				$('<option></option>')
				.text('-')
				.attr({ value: '' })
				.appendTo(el);

				for (age = 0; age <= 12; age++) {
					$('<option></option>')
					.text(age)
					.attr({ value: age })
					.appendTo(el);
				}
			}
		}
		else if (num < cur) {
			for (i = cur; i > num; i--) {
				sels.eq(i - 1).remove();
			}
		}

	});

});

/* Slider */

$(function() {

	var sliders = $('.main-header');

	if (!sliders.length) {
		return;
	}

	var nextSlide = function(slider) {

		var sel = '.slide-control',
			current = slider.find(sel + ':checked'),
			next;

		if (current.length) {
			next = current.nextAll(sel + ':first');
		}

		if (!next || !next.length) {
			next = slider.find(sel + ':first');
		}

		next.prop('checked', true);

	};

	var stopTimer = function(slider) {

		var timer = slider.data('_slider_timer');

		if (!timer) {
			return;
		}

		window.clearInterval(timer);
		slider.data('_slider_timer', false);

	};

	var startTimer = function(slider) {

		var msec = 6 * 1000;

		stopTimer(slider);
		slider.data('_slider_timer', window.setInterval(function() {
			nextSlide(slider);
		}, msec));

	};

	sliders.each(function() {
		startTimer($(this));
	});

});
