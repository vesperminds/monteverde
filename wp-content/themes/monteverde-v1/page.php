<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<?php while (have_posts()): the_post() ?>

<?php
	$images = mvt_get_hero_images();
?>
<?php if (!empty($images['desktop'])): ?>
<style type="text/css">
	.main-header-<?php the_ID(); ?> {
		background-image: url('<?= $images['desktop'] ?>') !important;
	}

	<?php if (!empty($images['mobile'])): ?>
	@media only screen and (max-width: 40em) {
		.main-header-<?php the_ID(); ?> {
			background-image: url('<?= $images['mobile'] ?>') !important;
		}
	}
	<?php endif; ?>
</style>
<?php endif; ?>

<header class="main-header main-header-<?php the_ID(); ?>">

	<?php get_template_part('site-nav'); ?>

	<div class="header-label post-header-label">
		<h1><?php the_title(); ?></h1>
	</div>

</header>

<div class="main-container">

	<div class="inner-info-content">

		<?php the_content(); ?>

		<a href="#" onclick="history.go(-1); return false;" class="btn-seemore btn-back"><?= __('Back', 'monteverde-th') ?></a>

	</div>

</div>

<?php break; endwhile; ?>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>
