<?php defined('ABSPATH') or die; ?>
<!DOCTYPE html>
<html class="Monteverde" <?php language_attributes(); ?> xmlns:ng="http://angularjs.org" ng-app="app">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title><?php mvt_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="msapplication-tap-highlight" content="no">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto:100,300,700,900,500' rel='stylesheet' type='text/css'>

	<link rel="shortcut icon" type="image/png" href="<?= get_template_directory_uri() ?>/favicon.ico"/>
	<link rel="shortcut icon" type="image/png" href="<?= get_template_directory_uri() ?>/favicon.ico"/>

	<script type="text/javascript">
		var _BASEPATH = <?= json_encode(rtrim(get_site_url(null, '/'), '/')) ?>
	</script>

	<?php wp_head(); ?>
</head>
<body <?= body_class() ?> ng-controller="mainCtrl">

	<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '',
			xfbml      : true,
			version    : 'v2.4'
		});
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/<?= __('en_US', 'locale for js libraries', 'monteverde-th') ?>/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

	<script type="text/javascript">
	window.gpAsyncInit = function() {

	};

	(function() {
		var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		po.src = 'https://apis.google.com/js/plusone.js?onload=gpAsyncInit';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
	</script>
