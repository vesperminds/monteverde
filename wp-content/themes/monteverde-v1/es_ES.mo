��    D      <  a   \      �     �     �     �               %     >     K     P     Y     `     v          �     �     �     �  
   �     �     �     �     �          &  
   )     4  	   <     F     U     b     p     }  9   �  �   �     Q  	   ^     h     q     ~  ~   �  �   	  U   �	  0   �	  T   
     n
     v
     �
     �
  h   �
     
          2     O     U     l  
   y     �  5   �  _   �     5     B     Q     k  "   |     �     �  "   �  e  �     [     l     ~     �     �     �     �     �     �     �          !     (     >     D     M     R     c     j     �     �     �     �     �     �     �     �     �               +  
   @  <   K  �   �       	   &     0     9     H  ~   [  �   �  d   h  2   �  `         a  
   i     t     x  x   �  	   �           $     6  	   <     F     L     P  7   a  Z   �  	   �     �     
  	   !     +     >     N  
   U         !   0   &       -   9             6   3   8      .                             *   )   :      ;   D   1                        2         	       B              /                  ,   %                 7               $                     +       (   #          
      5      ?      C   @      A            '   4          >   "              <   =       &laquo; Prev &raquo; Next * Required fields Adults All price ranges Any or no certifications Arrival Date Back Book Now Cancel Cancellation Policies Children Children's ages: Email Facebook Fax: Features First Name Follow us on Facebook Follow us on Google+ Follow us on Instagram Follow us on Pinterest Follow us on Twitter Go Go to Home Google+ Last Name Map Info (PDF) Next &raquo; Page %s of %s Phone Number Phone: Please enter any special requests you would like to make: Please fill the following fields in order to process your reservation. After completing the requested info, you will receive a confirmation email. Price Range: Read More See More Send booking Sustainable Tourism Thank you for signing up our newsletter. Your email address will be kept confidential and will not be shared with anyone else. Thanks for registering to visit %s. We've received all your information. You will receive a confirmation email in a few minutes. There are no places matching your criteria. Please broaden your search and try again. There's no articles yet! Please come back later. This place adheres to Act 7600 of Equal Opportunities for Persons with Disabilities. Twitter URL de politicas/en/policies View View by We have much to offer you! Return to the home page and discover all the places that are waiting for you. Website You are booking now to visit %s ajax pagingMore articles... en_US footerCONNECT WITH US footerEmail footerFax footerJOIN OUR NEWSLETTER footerJoin our online community and stay up to date. footerJoin to our newsletter to get free content delivered automatically each time we publish. footerPhone footerSign up footerYour email address footerYour name home page titleExplore Monteverde menu name in adminMain Menu not found titleOops! welcome title on home pageWelcome Project-Id-Version: 
POT-Creation-Date: 2016-05-09 20:07-0300
PO-Revision-Date: 2016-05-09 20:07-0300
Last-Translator: Emi <edgardo.balbuena@majesticmedia.ca>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 &laquo; Anterior &raquo; Siguiente * Campos obligatorios Adultos Todos los rangos de precio Cualquiera o sin certificación Fecha de llegada Atras Reservar ahora Cancelar Políticas de Cancelación Niños Edades de los niños: Email Facebook Fax: Características Nombre Síguenos en Facebook Síguenos en Google+ Síguenos en Instagram Síguenos en Pinterest Síguenos en Twitter Ir Ir a Inicio Google+ Apellido Info Mapa (PDF) Siguiente &raquo; Página %s de %s Número de teléfono Teléfono: Por favor ingrese cualquier pedido especial que desee hacer: Por favor llene los campos a continuación para procesar su reserva. Luego de completar el formulario, recibirá un email de confirmación. Rango de precios: Leer más Ver más Enviar reserva Turismo Sostenible Gracias por subscribirse a nuestra lista de correos. Su email se mantendrá confidencial y no será compartido con nadie más. Gracias por registrarte para visitar %s. Hemos recibido toda tu información. Recibirás un correo de confirmación en los próximos minutos. No hay lugares que coincidan con tu búsqueda. Por favor amplía los criterios e intenta nuevamente. ¡Aún no hay artículos! Por favor, vuelve luego. Este lugar se adhiere a la Ley 7600 de Igualdad de Oportunidades para Personas con Discapacidad. Twitter /politicas Ver Ver por ¡Tenemos muchísimo para ofrecerte! Vuelva a la página de inicio y descubre todos los lugares que te están esperando. Sitio web Estas reservando para visitar %s Mas artículos... es_CR CONECTATE Email Fax LISTA DE CORREOS Únete a nuestra comunidad online y mantente informado. Únete a nuestra lista de correos para recibir contenido gratuito cada vez que publicamos. Teléfono Inscribirse Tu dirección de email Tu nombre Explora Monteverde Menú principal ¡Ups! Bienvenido 