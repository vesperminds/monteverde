<?php defined('ABSPATH') or die;

function mvt_url($path = '/') {
	return get_stylesheet_directory_uri() . $path;
}

function mvt_ver($path = '/') {
	$file = get_stylesheet_directory() . $path;

	if (is_readable($file)) {

		$ver = (string) filemtime($file);

		if (!$ver) {
			$ver = date('YmdH');
		}

	}
	else {
		$ver = date('YmdH');
	}

	return $ver;
}

function mvt_the_slug() {
	global $post;

	if (is_single() || is_page()) {
		return $post->post_name;
	}

	return null;
}

function mvt_get_the_main_category_name() {

	$categories = get_the_category();

	if (empty($categories)) {
		return '';
	}

	return $categories[0]->name;

}

function mvt_json($data = null) {
	return htmlentities(json_encode($data), ENT_QUOTES);
}

/* Styles */

add_action('wp_enqueue_scripts', function() {

	/* Styles */

	wp_register_style(
		'main',
		mvt_url('/dist/css/main.css'),
		null,
		mvt_ver('/dist/css/main.css')
	);
	wp_enqueue_style('main');

	wp_register_style(
		'legacy',
		mvt_url('/css/legacy.css'),
		null,
		mvt_ver('/css/legacy.css')
	);
	wp_enqueue_style('legacy');

	/* Scripts – Deps */

	wp_deregister_script('jquery');
	wp_register_script(
		'jquery',
		mvt_url('/js/jquery-1.11.3.min.js'),
		null,
		mvt_ver('/js/jquery-1.11.3.min.js'),
		true
	);

	/* Scripts */

	wp_register_script(
		'main',
		mvt_url('/js/main.js'),
		[ 'jquery' ],
		mvt_ver('/js/main.js'),
		true
	);
	wp_enqueue_script('main');

});

/* Scripts */

/* Pages titles */

$_Monteverde_Override_Title = null;

function mvt_title_suffix() {
	return ' &mdash; Explore Monteverde';
}

function mvt_set_page_title($title = 'Explore Monteverde') {
	global $_Monteverde_Override_Title;

	if (empty($title)) {
		return;
	}

	$_Monteverde_Override_Title = $title;
}

function mvt_title() {
	return wp_title(mvt_title_suffix(), true, 'right');
}

add_filter('wp_title', function($title) {
	global $_Monteverde_Override_Title;

	if (!empty($_Monteverde_Override_Title)) {
		return $_Monteverde_Override_Title . mvt_title_suffix();
	}

	if (empty($title) && (is_home() || is_front_page())) {
	  return 'Explore Monteverde';
	}

	return $title;
});

function mvt_is_title_big($len = 40) {
	return strlen((string) get_the_title()) > $len;
}

/* Featured images support */

add_theme_support('post-thumbnails', [
	'post',
	'page',
	'place',
	'slide',
	'photo',
	'map',
]);

add_theme_support('menus');

add_action('init', function() {

	add_image_size('monteverde-hero-image', 1920, 1122);
	add_image_size('monteverde-mobile-hero-image', 960, 561);
	add_image_size('monteverde-post-thumbnail', 600, 600, true);
	add_image_size('monteverde-post-thumbnail-cover', 600, 600);
	add_image_size('monteverde-gallery-image', 1280, 1280);

});

/* Menus */

add_action('init', function() {

	register_nav_menu('main-menu', _x('Main Menu', 'menu name in admin', 'monteverde'));

});

/* AJAX support */

function mvt_is_ajax() {

	return !empty($_GET['ajax']);

}

/* Meta */

class MVT_Meta_Helper {

	protected $_data = [];

	public function __construct($meta) {
		$this->_data = (array) $meta;
	}

	public function get($key, $template = null, $idx = 0) {

		if (!isset($this->_data[$key]) || !isset($this->_data[$key][$idx])) {
			return null;
		}

		if ($template) {
			return sprintf($template, $this->_data[$key][$idx]);
		}

		return $this->_data[$key][$idx];

	}

	public function any(array $keys = []) {

		if (empty($keys)) {
			return false;
		}

		$output = false;

		for ($i = 0; $i < count($keys); $i++) {
			if (isset($this->_data[$keys[$i]])) {
				$output = true;
				break;
			}
		}

		return $output;

	}

}

function mvt_meta_helper($data = []) {

	if (!is_array($data)) {
		$data = [];
	}

	return new MVT_Meta_Helper($data);

}

/* Thumb images */

function mvt_get_hero_images($post_id = null) {

	$thumbnail_id = get_post_thumbnail_id($post_id);

	$desktop = wp_get_attachment_image_src($thumbnail_id, 'monteverde-hero-image');
	$mobile = wp_get_attachment_image_src($thumbnail_id, 'monteverde-mobile-hero-image');

	return [
		'desktop' => $desktop ? $desktop[0] : null,
		'mobile' => $mobile ? $mobile[0] : null
	];

}

/* Gallery */

function mvt_gallery_shortcode($attrs) {

	if (empty($attrs['ids'])) {
		return '';
	}

	$image_ids = array_map(function($id) {
		return (int) trim($id);
	}, explode(',', $attrs['ids']));

	$features = get_post_meta(get_the_ID(), 'features', true);

	if (!empty($features)) {
		$features = trim(preg_replace("/(\\r?\\n){1,}/", "\n", $features));
		$features = explode("\n", $features);
	}
	else {
		$features = [];
	}

	ob_start();
	include __DIR__ . '/elements/gallery-shortcode.php';

	return ob_get_clean();

}

add_shortcode('gallery', 'mvt_gallery_shortcode');

/* Maps */

add_shortcode('list_maps', function() {

	$mapsObj = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => 'map',
		'orderby' => 'title',
		'order' => 'ASC'
	]);

	$maps = [];

	if ($mapsObj->have_posts()) {
		while ($mapsObj->have_posts()) {

			$mapsObj->the_post();

			$thumbnail_id = get_post_thumbnail_id(get_the_ID());
			$thumb = '';

			if (!empty($thumbnail_id)) {
				$image = wp_get_attachment_image_src($thumbnail_id, 'monteverde-post-thumbnail-cover');

				if (!empty($image)) {
					$thumb = $image[0];
				}
			}

			$maps[] = [
				'title' => get_the_title(),
				'permalink' => get_post_meta(get_the_ID(), 'map-attachment', true),
				'thumb' => $thumb
			];

		}
	}

	wp_reset_postdata();

	ob_start();
	include __DIR__ . '/elements/list_maps-shortcode.php';

	return ob_get_clean();


});


/* Some helper shortcodes */

function mvt_kill_br($html) {
	return preg_replace('/\<\s*br\s*\/?\s*\>/i', '', $html);
}

add_shortcode('unbreakable', function($attr, $content = '') {

	return '<span class="unbreakable-text">' . do_shortcode($content) . '</span>';

});

add_shortcode('toptext', function($attr, $content = '') {

	return '<section class="top-text">' . do_shortcode(trim(mvt_kill_br($content))) . '</section>';

});

add_shortcode('toptext_title', function($attr, $content = '') {

	return '<h2>' . do_shortcode($content) . '</h2>';

});

add_shortcode('toptext_p', function($attr, $content = '') {

	return '<p>' . do_shortcode($content) . '</p>';

});

add_shortcode('section_title', function($attr, $content = '') {

	$output = '<h2 class="section-title"><strong>';
	$output .= do_shortcode(trim(mvt_kill_br($content)));
	$output .= '</strong><br/>';
	$output .= 'Monteverde</h2>';

	return $output;

});


/* Don't load language assets */

define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);
