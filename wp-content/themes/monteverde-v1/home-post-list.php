<?php if (!empty($HomePostListData) && $HomePostListData['query']->have_posts()): ?>

<?php if (!mvt_is_ajax()) { ?><div class="articles-info" id="HomePostListContainer"><?php } ?>

	<?php while ($HomePostListData['query']->have_posts()): $HomePostListData['query']->the_post() ?>
	<section>
		<?php
			$thumbnail_id = get_post_thumbnail_id();
			$image = wp_get_attachment_image_src($thumbnail_id, 'monteverde-post-thumbnail-cover')
		?>
		<?php if (!empty($image)): ?>
		<style type="text/css">
			.post-thumbnail-<?php the_ID(); ?> {
				background-image: url('<?= $image[0] ?>') !important;
			}
		</style>
		<?php endif; ?>
		<div class="post-thumbnail post-thumbnail-<?php the_ID(); ?>"></div>

		<article>

			<h2><?= mvt_get_the_main_category_name() ?></h2>
			<h3><?php the_title(); ?></h3>
			<p>
				<?= strip_tags(get_the_excerpt()) ?>
			</p>

			<a href="<?php the_permalink(); ?>" class="btn-seemore"><?= __('Read More', 'monteverde-th') ?></a>
			<!-- <a href="" class="info-square">
				<h4>Maps & <br> Directions</h4>
			</a> -->

		</article>

	</section>
	<?php endwhile; wp_reset_postdata(); ?>

<?php if (!mvt_is_ajax()) { ?></div><?php } ?>

<?php if (!mvt_is_ajax() && !$HomePostListData['end']): // button ?>
<div class="more-posts">
	<a href="" ng-click="ajaxLoadPosts(<?= mvt_json([
		'endpoint' => get_the_permalink(),
		'page' => $HomePostListData['paged'],
		'target' => '#HomePostListContainer'
	]) ?>)"><?= _x('More articles...', 'ajax paging', 'monteverde-th') ?></a>
</div>
<?php endif; ?>

<?php endif; ?>
