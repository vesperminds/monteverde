<?php

	if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_GET['book_now'])) {

		mv_book_now($_POST['email'], $_POST);

		exit;

	}

?>
<?php get_header(); ?>

<?php get_template_part('site-nav', 'mobile'); ?>

<?php while (have_posts()): the_post() ?>

<?php

	$thumb_id = get_post_thumbnail_id();
	$thumb = wp_get_attachment_image_src($thumb_id, 'monteverde-post-thumbnail-cover');

	$place_types = wp_get_post_terms(get_the_ID(), 'place_type', [ 'fields' => 'all' ]);

	if (!empty($place_types[0])) {
		$place_type = $place_types[0];
	}

	if (!empty($place_types[0])) {
		$image = z_taxonomy_image_url($place_type->term_id, 'monteverde-hero-image');
		$mobile_image = z_taxonomy_image_url($place_type->term_id, 'monteverde-mobile-hero-image');
	}

	$meta = mvt_meta_helper(get_post_custom(get_the_ID()));

?>
<?php if (!empty($image)): ?>
<style type="text/css">

	<?php if (!empty($thumb)): ?>
	.place-thumb-<?php the_ID() ?> {
		background-color: #ffffff;
		background-image: url('<?= $thumb[0] ?>') !important;
	}
	<?php endif; ?>

	<?php if (!empty($image)): ?>
	.main-header-<?php the_ID(); ?> {
		background-image: url('<?= $image ?>') !important;
	}

	@media only screen and (max-width: 40em) {
		.main-header-<?php the_ID(); ?> {
			background-image: url('<?= $mobile_image ?>') !important;
		}
	}
	<?php endif; ?>
</style>
<?php endif; ?>

<header class="main-header main-header-<?php the_ID(); ?>">

	<?php get_template_part('site-nav'); ?>

	<div class="header-label post-header-label">
		<h1>
			<?= $place_type->name ?><br>
			<strong>MONTEVERDE</strong>
		</h1>
		<p><?= $place_type->description ?></p>
	</div>

</header>

<div class="main-container">

	<div class="inner-info-content">

		<div class="place-title-and-thumb">

			<?php if (!empty($thumb)): ?>
			<div class="place-thumb place-thumb-<?php the_ID() ?>"></div>
			<?php endif; ?>

			<h2>
				<?php the_title(); ?>
				<?php if ($meta->any(['stars'])): ?>
					<sup class="stars"><?= str_repeat('&#9733', (int) $meta->get('stars')) ?></sup>
				<?php endif; ?>
			</h2>

		</div>


		<?php if ($meta->any(['short-description', 'address', 'phone', 'fax', 'email'])): ?>
		<div class="meta-info-wrapper">

			<?php if ($meta->any(['short-description'])): ?>
			<p class="meta-info meta-info-description">
				<strong><?= $meta->get('short-description') ?></strong>
			</p>
			<?php endif; ?>

			<?php if ($meta->any(['address'])): ?>
			<p class="meta-info meta-info-caps">
				<strong><?= $meta->get('address') ?></strong>
			</p>
			<?php endif; ?>

			<?php if ($meta->any(['phone', 'fax'])): ?>
			<p class="meta-info">
				<?= $meta->get('phone', __('Phone:', 'monteverde-th') . ' <strong>%s</strong>') ?>
				<?= $meta->get('phone', '&mdash; ' . __('Fax:', 'monteverde-th') . ' <strong>%s</strong>') ?>
			</p>
			<?php endif; ?>

			<?php if ($meta->any(['email'])): ?>
			<p class="meta-info">
				<a href="mailto:<?= $meta->get('email') ?>"><?= $meta->get('email') ?></a>
			</p>
			<?php endif; ?>

		</div>
		<?php endif; ?>

		<?php if ($meta->any(['website', 'facebook', 'twitter', 'google-plus'])): ?>
		<div class="meta-info-wrapper">

			<p class="meta-info meta-vertical-links">

				<?php if ($meta->any(['website'])): ?>
				<a target="_blank" rel="nofollow" href="<?= $meta->get('website') ?>"><?= __('Website', 'monteverde-th') ?></a>
				<?php endif; ?>

				<?php if ($meta->any(['facebook'])): ?>
					<a target="_blank" href="<?= $meta->get('facebook') ?>"><?= __('Facebook', 'monteverde-th') ?></a>
				<?php endif; ?>

				<?php if ($meta->any(['twitter'])): ?>
					<a target="_blank" href="<?= $meta->get('twitter') ?>"><?= __('Twitter', 'monteverde-th') ?></a>
				<?php endif; ?>

				<?php if ($meta->any(['google-plus'])): ?>
					<a target="_blank" href="<?= $meta->get('google-plus') ?>"><?= __('Google+', 'monteverde-th') ?></a>
				<?php endif; ?>

			</p>

		</div>
		<?php endif; ?>

		<?php if ($meta->any(['sustainable-tourism'])): ?>
		<div class="meta-info-wrapper">
			<?php if ($meta->any(['sustainable-tourism-link'])): ?>
				<a target="_blank" href="<?= $meta->get('sustainable-tourism-link') ?>">
					<img src="<?= mvt_url('/img/cst-' . $meta->get('sustainable-tourism') . '.jpg') ?>">
				</a>
			<?php else: ?>
				<img src="<?= mvt_url('/img/cst-' . $meta->get('sustainable-tourism') . '.jpg') ?>">
			<?php endif; ?>

		</div>
		<?php endif; ?>

		<?php if ($meta->any(['price-range'])): ?>
		<div class="meta-info-wrapper">
			<p class="meta-info">
				<?= __('Price Range:', 'monteverde-th') ?>
				<strong><?= str_repeat('$', (int) $meta->get('price-range')) ?></strong>
			</p>
		</div>
		<?php endif; ?>

		<?php the_content(); ?>

		<?php if ($meta->any(['act-7600'])): ?>
		<p class="act-7600-info">
			<img src="<?= mvt_url('/img/act-7600.png') ?>" alt="">
			<?= __('This place adheres to Act 7600 of Equal Opportunities for Persons with Disabilities.', 'monteverde-th') ?>
		</p>
		<?php endif; ?>

		<a href="#" onclick="history.go(-1); return false;" class="btn-seemore btn-back"><?= __('Back', 'monteverde-th') ?></a>

		<?php if ($meta->get('book-now')): ?>

			<a href="#" class="btn-book-now btn-seemore"><?= __('Book Now', 'monteverde-th') ?></a>

			<?php get_template_part('book-now-form') ?>

		<?php endif; ?>


	</div>

</div>

<?php break; endwhile; ?>

<?php get_template_part('site-footer'); ?>

<?php get_footer(); ?>
