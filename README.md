# Explore Monteverde

## Plugin de idioma

* Activar el plugin WPML Multilingual CMS
* Habilitar Español como idioma por defecto
* Agregar Inglés como idioma alternativo
* WPML > Opciones de traducción
** Habilitar para traducir Places (Lugares), Slides (Slides) y Photos (Fotos)
** Habilitar para traducir Place Types y Galleries
* WPML > Localización de temas y plugins
** Traducir utilizando archivos .mo
** Cargar el archivo .mo de tema de manera automática....: monteverde-th

## Apariencia

* Activar `Monteverde v1`


## Categorías

* Categorías de entradas en Entradas > Categorías
** Crear las categorías necesarias
** Crear las traducciones a las categorías

## Tipos de lugar

* Tipos de lugar en Lugars > Tipos de Lugar
** Crear los tipos de lugar necesarios
** Crear las traducciones a los tipos de lugar

## Galerías

* Galerías en Fotos > Galerías
** Crear las galerías necesarias
** Crear las traducciones a las galerías

## Banner de taxonomías (categorías y grupos de lugares)

* Activar el plugin Categories Images

## Ajustes de Wordpress

### Generales

* Cambiar `Descripción corta`
* Cambiar `Zona horaria` a `Costa Rica`

### Lectura

* Seleccionar `Una página estática`
** Página inicial: Inicio
** Página de entradas: Artículos
** 12 artículos por página

### Comentarios

* Deshabilitar `Permite que se publiquen comentarios en los artículos nuevos`

### Medios

* Cambiar `Tamaño de la miniatura` a 300x300
* Cambiar `Tamaño medio` a 600x600
* Cambiar `Tamaño grande` a 1280x1280

### Enlaces permanentes

* Estructura personalizada: `/%postname%-%post_id%`
* Categoría base: `sections` o similar

## Páginas

* Crear "Inicio" (/home)
* Crear "Artículos" (/articles)
* Crear "Galería" en español e inglés (/galería y /gallery)

## Menús

* Acomodar los menús en base al diseño
